#' Calculate the area of a circle given it's radius
#'
#' Circle area is calculated as pi*r^2
#'
#' @param r Radius of the circle, not unit-dependent
#'
#' @return Numeric vector of circle areas
#' @export
#'
#' @examples
#' area(1:4)
area <- function(r) {
  return(pi * r^2)
}
