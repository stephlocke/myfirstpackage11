
<!-- README.md is generated from README.Rmd. Please edit that file -->
myFirstPackage11
================

<!-- badges: start -->
[![Project Status: WIP - Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip) <!-- badges: end -->

The goal of myFirstPackage11 is to showcase package best practices and some simple functions.

Installation
------------

``` r
remotes::install_gitlab("stephlocke/myFirstPackage11")
```

This package will never be added to CRAN.

Example
-------

``` r
## basic example code
```

![Leslie Knope](https://media.giphy.com/media/ASUpZPfkfmv6g/giphy.gif)
